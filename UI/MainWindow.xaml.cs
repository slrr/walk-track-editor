﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using System.Windows.Media.Animation;
using System.Runtime.InteropServices;
using SlrrLib.View;

namespace WalkTrackEditor
{
  public partial class MainWindow : Window
  {
    [DllImport("kernel32.dll", ExactSpelling = true)]
    public static extern IntPtr GetConsoleWindow();

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool SetForegroundWindow(IntPtr hWnd);

    private SlrrLib.Model.TrcWalk currentHighlight = null;
    private SlrrLib.Geom.MapRpkModelFactory currentFactory = null;
    private IEnumerable<SlrrLib.Geom.NamedModel> spatialData = null;

    public event EventHandler MarkerPositionChangedByUser;
    public void OnMarkerPositionChangedByUser()
    {
      if (MarkerPositionChangedByUser != null)
        MarkerPositionChangedByUser.Invoke(this, new EventArgs());
    }

    public SlrrLib.Geom.WalkGeometry WalkGeom
    {
      get;
      set;
    } = new SlrrLib.Geom.WalkGeometry(null, "");
    public Vector3D MarkerPositionInRpkSpace
    {
      get
      {
        return ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace;
      }
    }
    public Vector3D MarkerPosition
    {
      get
      {
        return ctrlOrbitingViewport.MarkerPosition;
      }
      set
      {
        ctrlOrbitingViewport.LookAtPosition(value);
      }
    }

    public MainWindow()
    {
      InitializeComponent();
      ctrlWalkControl.MainWindowParent = this;
      if (System.IO.File.Exists("lastDir"))
        ctrlOrbitingViewport.LastRpkDirectory = System.IO.File.ReadAllText("lastDir");
      SlrrLib.Model.MessageLog.SetConsoleLogOutput();
      ctrlOrbitingViewport.IsViewDistanceManaged = true;
      ctrlOrbitingViewport.ObjectViewDistance = 1000;
      ctrlOrbitingViewport.MarkerMoved += ctrlOrbitingViewport_MarkerMoved;
      ctrlOrbitingViewport.MarkerMovedByUser += ctrlOrbitingViewport_MarkerMovedByUser;
    }

    private bool matchingNativeWalk(string rsdStr)
    {
      var spl = rsdStr.Split(new char[] { '\r', '\n', ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
      if (spl.Length >= 2)
      {
        return spl[0].ToLower() == "native" && spl[1].ToLower() == "walk";
      }
      return false;
    }
    private void recreateWalksInRPK()
    {
      var dlg = new Microsoft.Win32.SaveFileDialog
      {
        InitialDirectory = ctrlOrbitingViewport.LastRpkDirectory,
        FileName = "",
        DefaultExt = ".rpk",
        Filter = "rpk|*.rpk"
      };
      var result = dlg.ShowDialog();
      if (result == true)
      {
        SetForegroundWindow(GetConsoleWindow());
        WalkGeom.WalkMngr.ReBakeWalks(dlg.FileName);
      }
      else
      {
        return;
      }
    }
    private void loadSceneRpk()
    {
      ctrlOrbitingViewport.LoadSceneRpk();
      if(currentFactory == null)
        return;
      Title = ctrlOrbitingViewport.LastRpkOpened;
      WalkGeom = new SlrrLib.Geom.WalkGeometry(new SlrrLib.Model.DynamicRpk(currentFactory.RpkLoaded), ctrlOrbitingViewport.LastRpkOpened);
      WalkGeom.GenerateVisuals();
      foreach (var trcObj in WalkGeom.TrcWalkToGeom)
      {
        ctrlOrbitingViewport.Add3DRepresentationToScene(trcObj.Value, "trc-walk");
      }
      ctrlOrbitingViewport.UpdateVisibleObjectsViewDistance();
    }
    private void manageWalkMouseMove()
    {
      if (WalkGeom != null && !ctrlWalkControl.SelectionLocked)
      {
        var ClosestTrcObj = WalkGeom.GetClosestEntity(ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace);
        if (ClosestTrcObj != currentHighlight)
        {
          WalkGeom.ClearHighLight();
          WalkGeom.TopHighLightDynamicNamedSpatialData(ClosestTrcObj);
          currentHighlight = ClosestTrcObj;
          ctrlWalkControl.SetCurrentWalk(currentHighlight);
        }
      }
    }
    private void removeSpatialDataStructVisualisation()
    {
      if (spatialData == null)
        return;
      foreach (var model in spatialData)
        ctrlOrbitingViewport.RemoveModelFromScene(model);
    }
    private void addSpatialDataStructVisualisation()
    {
      removeSpatialDataStructVisualisation();
      spatialData = currentFactory.GetSpatialRepresentationForPoint(ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace).Select(x => new SlrrLib.Geom.NamedModel
      {
        ModelGeom = x,
        Name = "Spatial struct box",
        Translate = new Vector3D(x.Bounds.X, x.Bounds.Y, x.Bounds.Z)
      }).ToList();
      foreach (var model in spatialData)
        ctrlOrbitingViewport.AddModelToScene(model);
    }

    private void ctrlOrbitingViewport_MarkerMoved(object sender, EventArgs e)
    {
      manageWalkMouseMove();
    }
    private void ctrlOrbitingViewport_MarkerMovedByUser(object sender, EventArgs e)
    {
      OnMarkerPositionChangedByUser();
      manageWalkMouseMove();
    }
    private void ctrlButtonRenderScene_Click(object sender, RoutedEventArgs e)
    {
      loadSceneRpk();
    }
    private void ctrlButtonBakeWalks_Click(object sender, RoutedEventArgs e)
    {
      recreateWalksInRPK();
    }
    private void ctrlButtonAddToScene_Click(object sender, RoutedEventArgs e)
    {
      ctrlOrbitingViewport.LoadSecondaryRpk();
    }
    private void ctrlButtonResolveSpatialStruct_Click(object sender, RoutedEventArgs e)
    {
      addSpatialDataStructVisualisation();
    }
    private void ctrlButtonDeleteSpatialStruct_Click(object sender, RoutedEventArgs e)
    {
      removeSpatialDataStructVisualisation();
    }
    private void ctrlButtonAddNonSplineNode_Click(object sender, RoutedEventArgs e)
    {
      SlrrLib.Model.TrcWalk toad = new SlrrLib.Model.TrcWalk();
      var markerPos = ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace;
      toad.SpatialPosition = markerPos;
      toad.GlobalPavementPos = markerPos;
      Vector3D p1 = new Vector3D(3, 0, 0);
      Vector3D p2 = new Vector3D(0, 0, 3);
      Vector3D p3 = new Vector3D(-3, 0, 0);
      Vector3D p4 = new Vector3D(0, 0, -3);
      var line1 = new SlrrLib.Model.TrcWalkSurfaceSegment();
      line1.Type = 21;
      line1.Direction.Pos1 = p1;
      line1.Direction.Pos2 = p2;
      var line2 = new SlrrLib.Model.TrcWalkSurfaceSegment();
      line2.Type = 21;
      line2.Direction.Pos1 = p4;
      line2.Direction.Pos2 = p1;
      var line3 = new SlrrLib.Model.TrcWalkSurfaceSegment();
      line3.Type = 21;
      line3.Direction.Pos1 = p3;
      line3.Direction.Pos2 = p4;
      var line4 = new SlrrLib.Model.TrcWalkSurfaceSegment();
      line4.Type = 21;
      line4.Direction.Pos1 = p2;
      line4.Direction.Pos2 = p3;
      toad.AdjacencyDirections.Add(line1);
      toad.AdjacencyDirections.Add(line2);
      toad.AdjacencyDirections.Add(line3);
      toad.AdjacencyDirections.Add(line4);
      WalkGeom.WalkMngr.Walks.Add(toad);
      WalkGeom.TrcWalkToGeom[toad] = WalkGeom.GetGeomFromWalk(toad);
      WalkGeom.SetTransformsOf(toad);
      ctrlOrbitingViewport.Add3DRepresentationToScene(WalkGeom.TrcWalkToGeom[toad], "trc-walk");
      ctrlOrbitingViewport.UpdateVisibleObjectsViewDistance();
    }
    private void ctrlButtonAddSplineNode_Click(object sender, RoutedEventArgs e)
    {
      SlrrLib.Model.TrcWalk toad = new SlrrLib.Model.TrcWalk();
      var markerPos = ctrlOrbitingViewport.MarkerPositionInRpkSpatialSpace;
      toad.SpatialPosition = markerPos;
      Vector3D p1 = new Vector3D(3, 0, 0) + markerPos;
      Vector3D p2 = new Vector3D(0, 0, 3) + markerPos;
      Vector3D p3 = new Vector3D(-3, 0, 0) + markerPos;
      Vector3D p4 = new Vector3D(0, 0, -3) + markerPos;
      var line1 = new SlrrLib.Model.TrcWalkSurfaceSegment();
      line1.Type = 21;
      line1.Direction.Pos1 = p1;
      line1.Direction.Pos2 = p2;
      var line2 = new SlrrLib.Model.TrcWalkSurfaceSegment();
      line2.Type = 23;
      line2.Direction.Pos1 = p4;
      line2.Direction.Pos2 = p1;
      line2.ControlPoints.Add(new SlrrLib.Model.TrcWalkControlPoint3D(p4, (p1 - p4) * 0.1));
      line2.ControlPoints.Add(new SlrrLib.Model.TrcWalkControlPoint3D(p1, (p1 - p4) * 0.1));
      var line3 = new SlrrLib.Model.TrcWalkSurfaceSegment();
      line3.Type = 21;
      line3.Direction.Pos1 = p3;
      line3.Direction.Pos2 = p4;
      var line4 = new SlrrLib.Model.TrcWalkSurfaceSegment();
      line4.Type = 23;
      line4.Direction.Pos1 = p2;
      line4.Direction.Pos2 = p3;
      line4.ControlPoints.Add(new SlrrLib.Model.TrcWalkControlPoint3D(p2, (p3 - p2) * 0.1));
      line4.ControlPoints.Add(new SlrrLib.Model.TrcWalkControlPoint3D(p3, (p3 - p2) * 0.1));
      toad.AdjacencyDirections.Add(line1);
      toad.AdjacencyDirections.Add(line2);
      toad.AdjacencyDirections.Add(line3);
      toad.AdjacencyDirections.Add(line4);
      WalkGeom.WalkMngr.Walks.Add(toad);
      WalkGeom.TrcWalkToGeom[toad] = WalkGeom.GetGeomFromWalk(toad);
      WalkGeom.SetTransformsOf(toad);
      ctrlOrbitingViewport.Add3DRepresentationToScene(WalkGeom.TrcWalkToGeom[toad], "trc-walk");
      ctrlOrbitingViewport.UpdateVisibleObjectsViewDistance();
    }
    private void ctrlButtonDeleteNode_Click(object sender, RoutedEventArgs e)
    {
      if (currentHighlight == null)
        return;
      foreach (var adj in currentHighlight.AdjacencyDirections)
      {
        if (adj.Direction.OtherWalk != null)
        {
          foreach (var far in adj.Direction.OtherWalk.AdjacencyDirections)
          {
            if (far.Direction.OtherWalk == currentHighlight)
              far.Direction.OtherWalk = null;
          }
        }
      }
      ctrlOrbitingViewport.RemoveAllModelsBy3DRepresentation(WalkGeom.TrcWalkToGeom[currentHighlight]);
      WalkGeom.WalkMngr.Walks.Remove(currentHighlight);
      WalkGeom.TrcWalkToGeom.Remove(currentHighlight);
      ctrlOrbitingViewport.UpdateVisibleObjectsViewDistance();
    }
  }
}
