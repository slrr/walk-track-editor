﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WalkTrackEditor.View;

namespace WalkTrackEditor
{
  public partial class WalkSegmentControl : UserControl
  {
    private double dotProductLimit = 0.66;
    private bool disabled = true;
    private WalkSegmentView currentSegmentView;

    public WalkTrackEditor.MainWindow MainWindowParent
    {
      get;
      set;
    }

    public WalkSegmentControl()
    {
      InitializeComponent();
    }

    public void UpdateRelatedVisuals()
    {
      MainWindowParent.WalkGeom.WalkMngr.UpdateAdjacencyRefsOfWalk(currentSegmentView.context);
      MainWindowParent.WalkGeom.UpdateRelatedVisuals(currentSegmentView.context);
    }

    public event EventHandler PositionSelectedToMove;
    public void OnPositionSelectedToMove()
    {
      if (PositionSelectedToMove != null)
        PositionSelectedToMove.Invoke(this, new EventArgs());
    }

    public void DisableSnappingPart()
    {
      ctrlGridSnappingPart.IsEnabled = false;
      ctrlGridSnappingPart.Visibility = Visibility.Collapsed;
    }
    public void EnableSnappingPart()
    {
      ctrlGridSnappingPart.IsEnabled = true;
      ctrlGridSnappingPart.Visibility = Visibility.Visible;
    }
    public void DisableInteractions()
    {
      disabled = true;
      ctrlListBoxType23MoveSelector.SelectedIndex = -1;
      ctrlListBoxSrcOrTarget.SelectedIndex = -1;
    }
    public void SetCurrentSegmentView(WalkSegmentView newView)
    {
      disabled = false;
      currentSegmentView = newView;
      MainWindowParent.WalkGeom.WalkMngr.UpdateAdjacencyRefsOfWalk(currentSegmentView.context);
      currentSegmentView.RecalcRelatedRefs();
      updateMarkerPosition();
      MainWindowParent.WalkGeom.UpdateRelatedVisuals(currentSegmentView.context);

      ctrlType23SplineSegments.Items.Clear();
      ctrlListBoxSrcOrTarget.Items.Clear();
      ListBoxItem toad = new ListBoxItem
      {
        Foreground = Brushes.Green,
        Content = "MoveSrc"
      };
      ctrlListBoxSrcOrTarget.Items.Add(toad);
      toad = new ListBoxItem
      {
        Foreground = Brushes.Blue,
        Content = "MoveTarget"
      };
      ctrlListBoxSrcOrTarget.Items.Add(toad);
      if (currentSegmentView.managedSegment.Type == 23)
      {
        ctrlGridType23.IsEnabled = true;
        for (int i = 1; i < currentSegmentView.managedSegment.ControlPoints.Count - 1; ++i)
        {
          ctrlType23SplineSegments.Items.Add(currentSegmentView.managedSegment.ControlPoints[i]);
        }
        toad = new ListBoxItem
        {
          Foreground = Brushes.Green,
          Content = "MoveSrcNormal"
        };
        ctrlListBoxSrcOrTarget.Items.Add(toad);
        toad = new ListBoxItem
        {
          Foreground = Brushes.Blue,
          Content = "MoveTargetNormal"
        };
        ctrlListBoxSrcOrTarget.Items.Add(toad);
      }
      else
      {
        ctrlGridType23.IsEnabled = false;
      }
    }
    public void SubscribeToMarkerMovement()
    {
      MainWindowParent.MarkerPositionChangedByUser += ctrlMainWindowParent_MarkerPositionChangedByUser;
    }

    private void updateMarkerPosition()
    {
      if (disabled)
        return;
      if (MainWindowParent == null)
        return;
      if (ctrlListBoxSrcOrTarget.SelectedIndex == 0)//MoveSource
      {
        MainWindowParent.MarkerPosition = currentSegmentView.GlobalSourcePosition;
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 1)
      {
        MainWindowParent.MarkerPosition = currentSegmentView.GlobalTargetPosition;
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 2)//move src normal
      {
        if (currentSegmentView.managedSegment.ControlPoints.Any())
          MainWindowParent.MarkerPosition = currentSegmentView.managedSegment.ControlPoints.First().Normal +
                                            currentSegmentView.managedSegment.ControlPoints.First().Position;
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 3)
      {
        if (currentSegmentView.managedSegment.ControlPoints.Any())
          MainWindowParent.MarkerPosition = currentSegmentView.managedSegment.ControlPoints.Last().Normal +
                                            currentSegmentView.managedSegment.ControlPoints.Last().Position;
      }
      else if (ctrlListBoxType23MoveSelector.SelectedIndex == 0)
      {
        if (ctrlType23SplineSegments.SelectedItem is SlrrLib.Model.TrcWalkControlPoint3D curSplinePos)
        {
          MainWindowParent.MarkerPosition = curSplinePos.Position;
        }
      }
      else if (ctrlListBoxType23MoveSelector.SelectedIndex == 1)
      {
        if (ctrlType23SplineSegments.SelectedItem is SlrrLib.Model.TrcWalkControlPoint3D curSplinePos)
        {
          MainWindowParent.MarkerPosition = curSplinePos.Position + curSplinePos.Normal;
        }
      }
    }
    private SlrrLib.Model.TrcWalkControlPoint3D getOppositePoint(SlrrLib.Model.TrcWalkControlPoint3D oneSide)
    {
      if (currentSegmentView.managedSegment.Type != 23)
        return null;
      var other = currentSegmentView.context.GetNextSplineAdjacency(currentSegmentView.managedSegment);
      int indInManaged = currentSegmentView.managedSegment.ControlPoints.IndexOf(oneSide);
      return other.ControlPoints[other.ControlPoints.Count - indInManaged - 1];
    }
    private SlrrLib.Model.TrcWalkControlPoint3D getOppositePoint(SlrrLib.Model.TrcWalkControlPoint3D oneSide, SlrrLib.Model.TrcWalkSurfaceSegment oneSegment)
    {
      if (oneSegment.Type != 23)
        return null;
      var other = currentSegmentView.context.GetNextSplineAdjacency(oneSegment);
      int indInManaged = oneSegment.ControlPoints.IndexOf(oneSide);
      return other.ControlPoints[other.ControlPoints.Count - indInManaged - 1];
    }

    private void enforceWidthConstraint(float width)
    {
      if (!currentSegmentView.context.HasType23Adjacency())
        return;
      var adj1 = currentSegmentView.context.AdjacencyDirections[0];
      var spl1 = currentSegmentView.context.AdjacencyDirections[1];
      var adj2 = currentSegmentView.context.AdjacencyDirections[2];
      for (int i = 0; i != spl1.ControlPoints.Count; ++i)
      {
        var control = spl1.ControlPoints[i];
        var other = getOppositePoint(control, spl1);
        var otherToMe = control.Position - other.Position;
        otherToMe.Normalize();
        otherToMe *= width;
        otherToMe += other.Position;
        control.Position = otherToMe;
      }
      spl1.Direction.Pos1 = spl1.ControlPoints.First().Position;
      spl1.Direction.Pos2 = spl1.ControlPoints.Last().Position;
      adj1.Direction.Pos2 = spl1.Direction.Pos1;
      adj2.Direction.Pos1 = spl1.Direction.Pos2;
    }
    private void ctrlMainWindowParent_MarkerPositionChangedByUser(object sender, EventArgs e)
    {
      if (disabled)
        return;
      var markerPos = MainWindowParent.MarkerPositionInRpkSpace;
      if (ctrlListBoxSrcOrTarget.SelectedIndex == 0)//MoveSource
      {
        currentSegmentView.GlobalSourcePosition = markerPos;
        if (currentSegmentView.managedSegment.Type == 23)
        {
          var other = getOppositePoint(currentSegmentView.managedSegment.ControlPoints.First());
          other.Position = new Vector3D(other.Position.X,currentSegmentView.managedSegment.ControlPoints.First().Position.Y,other.Position.Z);
        }
        MainWindowParent.WalkGeom.UpdateRelatedVisuals(currentSegmentView.context);
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 1)
      {
        currentSegmentView.GlobalTargetPosition = markerPos;
        if (currentSegmentView.managedSegment.Type == 23)
        {
          var other = getOppositePoint(currentSegmentView.managedSegment.ControlPoints.Last());
          other.Position = new Vector3D(other.Position.X,currentSegmentView.managedSegment.ControlPoints.Last().Position.Y,other.Position.Z);
        }
        MainWindowParent.WalkGeom.UpdateRelatedVisuals(currentSegmentView.context);
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 2)//move src normal
      {
        var normalValue = markerPos - currentSegmentView.managedSegment.ControlPoints.First().Position;
        normalValue.Normalize();
        var direction = currentSegmentView.managedSegment.ControlPoints.First().Position -
                        currentSegmentView.managedSegment.ControlPoints.Last().Position;
        direction.Normalize();
        if (Math.Abs(Vector3D.DotProduct(normalValue, direction)) > dotProductLimit)
        {
          currentSegmentView.managedSegment.ControlPoints.First().Normal = markerPos - currentSegmentView.managedSegment.ControlPoints.First().Position;
          var opposite = getOppositePoint(currentSegmentView.managedSegment.ControlPoints.First());
          opposite.Normal = new Vector3D(-currentSegmentView.managedSegment.ControlPoints.First().Normal.X,opposite.Normal.Y,-currentSegmentView.managedSegment.ControlPoints.First().Normal.Z);
          MainWindowParent.WalkGeom.UpdateRelatedVisuals(currentSegmentView.context);
        }
      }
      else if (ctrlListBoxSrcOrTarget.SelectedIndex == 3)
      {
        var normalValue = markerPos - currentSegmentView.managedSegment.ControlPoints.Last().Position;
        normalValue.Normalize();
        var direction = currentSegmentView.managedSegment.ControlPoints.First().Position -
                        currentSegmentView.managedSegment.ControlPoints.Last().Position;
        direction.Normalize();
        if (Math.Abs(Vector3D.DotProduct(normalValue, direction)) > dotProductLimit)
        {
          currentSegmentView.managedSegment.ControlPoints.Last().Normal = markerPos - currentSegmentView.managedSegment.ControlPoints.Last().Position;
          var opposite = getOppositePoint(currentSegmentView.managedSegment.ControlPoints.Last());
          opposite.Normal = new Vector3D(-currentSegmentView.managedSegment.ControlPoints.Last().Normal.X,opposite.Normal.Y,-currentSegmentView.managedSegment.ControlPoints.Last().Normal.Z);
          MainWindowParent.WalkGeom.UpdateRelatedVisuals(currentSegmentView.context);
        }
      }
      else if (ctrlListBoxType23MoveSelector.SelectedIndex == 0)
      {
        if (ctrlType23SplineSegments.SelectedItem is SlrrLib.Model.TrcWalkControlPoint3D curSplinePos)
        {
          curSplinePos.Position = markerPos;
          var other = getOppositePoint(curSplinePos);
          other.Position = new Vector3D(other.Position.X,markerPos.Y,other.Position.Z);
          MainWindowParent.WalkGeom.UpdateRelatedVisuals(currentSegmentView.context);
        }
      }
      else if (ctrlListBoxType23MoveSelector.SelectedIndex == 1)
      {
        if (ctrlType23SplineSegments.SelectedItem is SlrrLib.Model.TrcWalkControlPoint3D curSplinePos)
        {
          var normalValue = markerPos - curSplinePos.Position;
          int curIndex = currentSegmentView.managedSegment.ControlPoints.IndexOf(curSplinePos);
          int nextIndex = (curIndex + 1) % currentSegmentView.managedSegment.ControlPoints.Count;
          var direction = curSplinePos.Position - currentSegmentView.managedSegment.ControlPoints[nextIndex].Position;
          direction.Normalize();
          normalValue.Normalize();
          if (Math.Abs(Vector3D.DotProduct(normalValue, direction)) > dotProductLimit)
          {
            curSplinePos.Normal = markerPos - curSplinePos.Position;
            var opposite = getOppositePoint(curSplinePos);
            opposite.Normal = new Vector3D(-curSplinePos.Normal.X,opposite.Normal.Y,-curSplinePos.Normal.Z);
            MainWindowParent.WalkGeom.UpdateRelatedVisuals(currentSegmentView.context);
          }
        }
      }
    }
    private void ctrlListBoxSrcOrTarget_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (ctrlListBoxSrcOrTarget.SelectedIndex == -1)
        return;
      if (disabled)
        return;
      if (ctrlListBoxType23MoveSelector != null)
        ctrlListBoxType23MoveSelector.SelectedIndex = -1;
      updateMarkerPosition();
      OnPositionSelectedToMove();
    }
    private void ctrlListBoxType23MoveSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (ctrlListBoxType23MoveSelector.SelectedIndex == -1)
        return;
      disabled = false;
      if (ctrlListBoxSrcOrTarget != null)
        ctrlListBoxSrcOrTarget.SelectedIndex = -1;
      updateMarkerPosition();
      OnPositionSelectedToMove();
    }
    private void ctrlButtonSnap_Click(object sender, RoutedEventArgs e)
    {
      if (disabled)
        return;
      var seg = currentSegmentView.managedSegment;
      var walk = currentSegmentView.context;
      seg.Direction.OtherWalk = null;
      currentSegmentView.RecalcRelatedRefs(true);
      foreach (var other in MainWindowParent.WalkGeom.WalkMngr.Walks)
      {
        if (other == currentSegmentView.context)
          continue;
        bool found = false;
        foreach (var otherSeg in other.AdjacencyDirections)
        {
          if (otherSeg.Type == 23)
            continue;
          if (SlrrLib.Model.TrcWalkManager.RoughEqual(walk.GetGlobalSourcePosition(seg), other.GetGlobalSourcePosition(otherSeg), 1.0) &&
              SlrrLib.Model.TrcWalkManager.RoughEqual(walk.GetGlobalTargetPosition(seg), other.GetGlobalTargetPosition(otherSeg), 1.0))
          {
            found = true;
            currentSegmentView.GlobalSourcePosition = other.GetGlobalSourcePosition(otherSeg);
            currentSegmentView.GlobalTargetPosition = other.GetGlobalTargetPosition(otherSeg);
            break;
          }
          if (SlrrLib.Model.TrcWalkManager.RoughEqual(walk.GetGlobalTargetPosition(seg), other.GetGlobalSourcePosition(otherSeg), 1.0) &&
              SlrrLib.Model.TrcWalkManager.RoughEqual(walk.GetGlobalSourcePosition(seg), other.GetGlobalTargetPosition(otherSeg), 1.0))
          {
            found = true;
            currentSegmentView.GlobalSourcePosition = other.GetGlobalTargetPosition(otherSeg);
            currentSegmentView.GlobalTargetPosition = other.GetGlobalSourcePosition(otherSeg);
            break;
          }
        }
        if (found)
        {
          MainWindowParent.WalkGeom.WalkMngr.UpdateAdjacencyRefsOfWalk(currentSegmentView.context);
          currentSegmentView.RecalcRelatedRefs();
          updateMarkerPosition();
          MainWindowParent.WalkGeom.UpdateRelatedVisuals(currentSegmentView.context);
          break;
        }
      }
    }
    private void ctrlButtonBreakSnap_Click(object sender, RoutedEventArgs e)
    {
      if (disabled)
        return;
      if (currentSegmentView.managedSegment.Direction.OtherWalk == null)
      {
        MessageBox.Show("Cant brake snapping, segment has no direct related segment.");
        return;
      }
      var seg = currentSegmentView.managedSegment;
      var walk = currentSegmentView.context;
      seg.Direction.OtherWalk = null;
      currentSegmentView.RecalcRelatedRefs(true);
    }
    private void ctrlButtonDelSplineSegment_Click(object sender, RoutedEventArgs e)
    {
      if (disabled)
        return;
      if (currentSegmentView.managedSegment.Type != 23)
        return;
      if (!(ctrlType23SplineSegments.SelectedItem is SlrrLib.Model.TrcWalkControlPoint3D selectedSplSegment))
        return;
      var indexToremove = ctrlType23SplineSegments.SelectedIndex + 1;
      if (indexToremove == 0)
        return;
      if (indexToremove == currentSegmentView.managedSegment.ControlPoints.Count - 1)
        return;
      var SplAdj1 = currentSegmentView.context.GetFirstSplineAdjacency();
      var SplAdj2 = currentSegmentView.context.GetLastSplineAdjacency();
      if (SplAdj1 == currentSegmentView.managedSegment)
      {
        SplAdj1.ControlPoints.RemoveAt(indexToremove);
        SplAdj2.ControlPoints.RemoveAt(SplAdj2.ControlPoints.Count - indexToremove - 1);
      }
      else
      {
        SplAdj2.ControlPoints.RemoveAt(indexToremove);
        SplAdj1.ControlPoints.RemoveAt(SplAdj2.ControlPoints.Count - indexToremove - 1);
      }
      var selItemBack = ctrlType23SplineSegments.SelectedItem;
      ctrlType23SplineSegments.SelectedIndex = ctrlType23SplineSegments.SelectedIndex - 1;
      ctrlType23SplineSegments.Items.Remove(selItemBack);
      UpdateRelatedVisuals();
    }
    private void ctrlButtonInsertSplineSegment_Click(object sender, RoutedEventArgs e)
    {
      if (disabled)
        return;
      if (currentSegmentView.managedSegment.Type != 23)
        return;
      var selectedSplSegment = ctrlType23SplineSegments.SelectedItem as SlrrLib.Model.TrcWalkControlPoint3D;
      if (selectedSplSegment == null && ctrlType23SplineSegments.Items.Count != 0)
        return;
      var indexToInsert = ctrlType23SplineSegments.SelectedIndex + 1;
      var mirrorIndex = currentSegmentView.managedSegment.ControlPoints.Count - 1 - indexToInsert;
      if (ctrlType23SplineSegments.SelectedIndex == -1)
      {
        indexToInsert = 0;
        mirrorIndex = 1;
      }
      else if (mirrorIndex == currentSegmentView.managedSegment.ControlPoints.Count - 1)
        return;
      var SplAdj1 = currentSegmentView.context.GetFirstSplineAdjacency();
      var SplAdj2 = currentSegmentView.context.GetLastSplineAdjacency();
      if (SplAdj1 == currentSegmentView.managedSegment)
      {
        SplAdj1.InsertHalfwayControlPointAfter(indexToInsert);
        SplAdj2.InsertHalfwayControlPointBefore(mirrorIndex);
      }
      else
      {
        SplAdj1.InsertHalfwayControlPointBefore(mirrorIndex);
        SplAdj2.InsertHalfwayControlPointAfter(indexToInsert);
      }
      ctrlType23SplineSegments.Items.Clear();
      for (int i = 1; i < currentSegmentView.managedSegment.ControlPoints.Count - 1; ++i)
      {
        ctrlType23SplineSegments.Items.Add(currentSegmentView.managedSegment.ControlPoints[i]);
      }
      UpdateRelatedVisuals();
    }
    private void ctrlType23SplineSegments_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (ctrlType23SplineSegments.SelectedIndex == -1)
        return;
      disabled = false;
      updateMarkerPosition();
      OnPositionSelectedToMove();
    }
  }
}
