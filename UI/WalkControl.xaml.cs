﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WalkTrackEditor.View;

namespace WalkTrackEditor
{
  public partial class WalkControl : UserControl
  {
    private WalkTrackEditor.MainWindow _mainWindowParent = null;
    private SlrrLib.Model.TrcWalk context;

    public bool SelectionLocked
    {
      get;
      set;
    } = false;

    public WalkTrackEditor.MainWindow MainWindowParent
    {
      get
      {
        return _mainWindowParent;
      }
      set
      {
        _mainWindowParent = value;
        if (ctrlSegmentView.MainWindowParent != value)
        {
          ctrlSegmentView.MainWindowParent = value;
          ctrlSegmentView.SubscribeToMarkerMovement();
        }
      }
    }

    public WalkControl()
    {
      InitializeComponent();
      ctrlSegmentView.IsEnabled = false;
    }

    public void SetCurrentWalk(SlrrLib.Model.TrcWalk walk)
    {
      if (walk == null)
        return;
      context = walk;
      ctrlListBoxAdjacencies.Items.Clear();
      foreach (var segment in walk.AdjacencyDirections)
        ctrlListBoxAdjacencies.Items.Add(segment);
    }
    public void LockSelection()
    {
      SelectionLocked = true;
      ctrlButtonSelectionLocking.Content = "SelectionIsLocked";
      ctrlButtonSelectionLocking.Background = Brushes.DarkRed;
    }
    public void UnLockSelection()
    {
      SelectionLocked = false;
      ctrlButtonSelectionLocking.Content = "LockSelection";
      ctrlButtonSelectionLocking.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0xDD, 0xDD, 0xDD));
    }

    private void ctrlListBoxAdjacencies_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!(ctrlListBoxAdjacencies.SelectedItem is SlrrLib.Model.TrcWalkSurfaceSegment adj))
        return;
      ctrlSegmentView.IsEnabled = true;
      ctrlSegmentView.SetCurrentSegmentView(new WalkSegmentView(MainWindowParent.WalkGeom.WalkMngr, context, adj));
      LockSelection();
      if (context.HasType23Adjacency() || context.AdjacencyDirections.Count == 3)
        ctrlSegmentView.EnableSnappingPart();
      else
      {
        if (context.AdjacencyDirections.IndexOf(adj) % 2 == 1)
          ctrlSegmentView.EnableSnappingPart();
        else
          ctrlSegmentView.DisableSnappingPart();
      }
    }
    private void ctrlButtonSelectionLocking_Click(object sender, RoutedEventArgs e)
    {
      SelectionLocked = !SelectionLocked;
      if (SelectionLocked)
      {
        LockSelection();
      }
      else
      {
        UnLockSelection();
        ctrlSegmentView.DisableInteractions();
      }
    }
    private void ctrlSegmentView_PositionSelectedToMove(object sender, EventArgs e)
    {
      LockSelection();
    }
    private void ctrlButtonDeselectAll_Click(object sender, RoutedEventArgs e)
    {
      ctrlSegmentView.IsEnabled = false;
      ctrlListBoxAdjacencies.SelectedIndex = -1;
      UnLockSelection();
    }
  }
}
