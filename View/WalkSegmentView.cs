﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using SlrrLib.Model;
using SlrrLib.View;
using Vec3 = System.Windows.Media.Media3D.Vector3D;

namespace WalkTrackEditor.View
{
  public class WalkSegmentView : NotifyPropertyChangedBase
  {
    private TrcWalkManager globalContext;

    public List<WalkSegmentView> mySrcPointToTheirSrcPoint = new List<WalkSegmentView>();
    public List<WalkSegmentView> myTargetToTheirSrcPoint = new List<WalkSegmentView>();
    public List<WalkSegmentView> myTargetToTheirTarget = new List<WalkSegmentView>();
    public List<WalkSegmentView> mySrcPointToTheirTarget = new List<WalkSegmentView>();
    public List<WalkSegmentView> myTargetYToTheirTargetY = new List<WalkSegmentView>();
    public List<WalkSegmentView> mySrcPointYToTheirTargetY = new List<WalkSegmentView>();
    public List<WalkSegmentView> myTargetYToTheirSrcPointY = new List<WalkSegmentView>();
    public List<WalkSegmentView> mySrcPointYToTheirSrcPointY = new List<WalkSegmentView>();
    public TrcWalkSurfaceSegment managedSegment;
    public TrcWalk context;

    public Vec3 GlobalSourcePosition
    {
      get
      {
        return context.GetGlobalSourcePosition(managedSegment);
      }
      set
      {
        context.SetGlobalSourcePoint(value,managedSegment);
        foreach (var other in mySrcPointToTheirSrcPoint)
          other.context.SetGlobalSourcePoint(value, other.managedSegment);
        foreach (var other in mySrcPointToTheirTarget)
          other.context.SetGlobalTargetPoint(value, other.managedSegment);

        foreach (var far in mySrcPointYToTheirSrcPointY)
        {
          var tmpPos = far.GlobalSourcePosition;
          tmpPos.Y = value.Y;
          far.GlobalSourcePosition = tmpPos;
        }
        foreach (var far in mySrcPointYToTheirTargetY)
        {
          var tmpPos = far.GlobalTargetPosition;
          tmpPos.Y = value.Y;
          far.GlobalTargetPosition = tmpPos;
        }

        OnPropertyChanged("GlobalSourcePosition");
      }
    }
    public Vec3 GlobalTargetPosition
    {
      get
      {
        return context.GetGlobalTargetPosition(managedSegment);
      }
      set
      {
        context.SetGlobalTargetPoint(value,managedSegment);
        foreach (var other in myTargetToTheirSrcPoint)
          other.context.SetGlobalSourcePoint(value, other.managedSegment);
        foreach (var other in myTargetToTheirTarget)
          other.context.SetGlobalTargetPoint(value, other.managedSegment);

        foreach (var far in myTargetYToTheirSrcPointY)
        {
          var tmpPos = far.GlobalSourcePosition;
          tmpPos.Y = value.Y;
          far.GlobalSourcePosition = tmpPos;
        }
        foreach (var far in myTargetYToTheirTargetY)
        {
          var tmpPos = far.GlobalTargetPosition;
          tmpPos.Y = value.Y;
          far.GlobalTargetPosition = tmpPos;
        }

        OnPropertyChanged("GlobalTargetPosition");
      }
    }

    public WalkSegmentView()
    {

    }
    public WalkSegmentView(TrcWalkManager globalContext,TrcWalk context, TrcWalkSurfaceSegment managedSegment)
    {
      this.context = context;
      this.managedSegment = managedSegment;
      this.globalContext = globalContext;
    }

    public void RecalcRelatedRefs(bool onlyCalcLocalRelations = false)
    {
      mySrcPointToTheirSrcPoint.Clear();
      myTargetToTheirSrcPoint.Clear();
      myTargetToTheirTarget.Clear();
      mySrcPointToTheirTarget.Clear();
      myTargetYToTheirTargetY.Clear();
      mySrcPointYToTheirTargetY.Clear();
      myTargetYToTheirSrcPointY.Clear();
      mySrcPointYToTheirSrcPointY.Clear();
      if (!onlyCalcLocalRelations)
      {
        foreach (var adj in context.AdjacencyDirections)
        {
          foreach (var other in globalContext.Walks)
          {
            if (other == context)
              continue;
            foreach (var otherSeg in other.AdjacencyDirections)
            {
              if (TrcWalkManager.RoughEqual(context.GetGlobalSourcePosition(managedSegment),
                                            other.GetGlobalTargetPosition(otherSeg)))
              {
                mySrcPointToTheirTarget.Add(new WalkSegmentView(globalContext, other, otherSeg));
                fallenInFollowingAtLeastOneIndirection(other, context, true);
              }
              if (TrcWalkManager.RoughEqual(context.GetGlobalSourcePosition(managedSegment),
                                            other.GetGlobalSourcePosition(otherSeg)))
              {
                mySrcPointToTheirSrcPoint.Add(new WalkSegmentView(globalContext, other, otherSeg));
                fallenInFollowingAtLeastOneIndirection(other, context, true);
              }
              if (TrcWalkManager.RoughEqual(context.GetGlobalTargetPosition(managedSegment),
                                            other.GetGlobalSourcePosition(otherSeg)))
              {
                myTargetToTheirSrcPoint.Add(new WalkSegmentView(globalContext, other, otherSeg));
                fallenInFollowingAtLeastOneIndirection(other, context, false);
              }
              if (TrcWalkManager.RoughEqual(context.GetGlobalTargetPosition(managedSegment),
                                            other.GetGlobalTargetPosition(otherSeg)))
              {
                myTargetToTheirTarget.Add(new WalkSegmentView(globalContext, other, otherSeg));
                fallenInFollowingAtLeastOneIndirection(other, context, false);
              }
            }
          }
        }
        if (!context.HasType23Adjacency())
        {
          fallenInFollowingAtLeastOneIndirection(context, context, false);
          fallenInFollowingAtLeastOneIndirection(context, context, true);
        }
      }
      foreach (var seg in context.AdjacencyDirections)
      {
        if (seg == managedSegment)
          continue;
        if (TrcWalkManager.RoughEqual(context.GetGlobalSourcePosition(managedSegment),
                                      context.GetGlobalTargetPosition(seg)))
        {
          mySrcPointToTheirTarget.Add(new WalkSegmentView(globalContext, context, seg));
        }
        if (TrcWalkManager.RoughEqual(context.GetGlobalSourcePosition(managedSegment),
                                      context.GetGlobalSourcePosition(seg)))
        {
          mySrcPointToTheirSrcPoint.Add(new WalkSegmentView(globalContext, context, seg));
        }
        if (TrcWalkManager.RoughEqual(context.GetGlobalTargetPosition(managedSegment),
                                      context.GetGlobalSourcePosition(seg)))
        {
          myTargetToTheirSrcPoint.Add(new WalkSegmentView(globalContext, context, seg));
        }
        if (TrcWalkManager.RoughEqual(context.GetGlobalTargetPosition(managedSegment),
                                      context.GetGlobalTargetPosition(seg)))
        {
          myTargetToTheirTarget.Add(new WalkSegmentView(globalContext, context, seg));
        }
      }

      var tmp = GlobalSourcePosition;
      GlobalSourcePosition = tmp;
      tmp = GlobalTargetPosition;
      GlobalTargetPosition = tmp;
    }

    private void fallenInFollowingAtLeastOneIndirection(TrcWalk farContext,TrcWalk middleContext, bool FallenThroughSource)
    {
      bool jmpAgain = !farContext.HasType23Adjacency();
      foreach (var far in farContext.AdjacencyDirections)
      {
        foreach (var middle in middleContext.AdjacencyDirections)
        {
          //there will be duplicates....
          if (TrcWalkManager.RoughEqual(middleContext.GetGlobalSourcePosition(middle),
                                        farContext.GetGlobalTargetPosition(far)))
          {
            if (FallenThroughSource)
            {
              mySrcPointYToTheirSrcPointY.Add(new WalkSegmentView(globalContext, middleContext, middle));
              mySrcPointYToTheirTargetY.Add(new WalkSegmentView(globalContext, farContext, far));
            }
            else
            {
              myTargetYToTheirSrcPointY.Add(new WalkSegmentView(globalContext, middleContext, middle));
              myTargetYToTheirTargetY.Add(new WalkSegmentView(globalContext, farContext, far));
            }
          }
          if (TrcWalkManager.RoughEqual(middleContext.GetGlobalSourcePosition(middle),
                                        farContext.GetGlobalSourcePosition(far)))
          {
            if (FallenThroughSource)
            {
              mySrcPointYToTheirSrcPointY.Add(new WalkSegmentView(globalContext, middleContext, middle));
              mySrcPointYToTheirSrcPointY.Add(new WalkSegmentView(globalContext, farContext, far));
            }
            else
            {
              myTargetYToTheirSrcPointY.Add(new WalkSegmentView(globalContext, middleContext, middle));
              myTargetYToTheirSrcPointY.Add(new WalkSegmentView(globalContext, farContext, far));
            }
          }
          if (TrcWalkManager.RoughEqual(middleContext.GetGlobalTargetPosition(middle),
                                        farContext.GetGlobalSourcePosition(far)))
          {
            if (FallenThroughSource)
            {
              mySrcPointYToTheirTargetY.Add(new WalkSegmentView(globalContext, middleContext, middle));
              mySrcPointYToTheirSrcPointY.Add(new WalkSegmentView(globalContext, farContext, far));
            }
            else
            {
              myTargetYToTheirTargetY.Add(new WalkSegmentView(globalContext, middleContext, middle));
              myTargetYToTheirSrcPointY.Add(new WalkSegmentView(globalContext, farContext, far));
            }
          }
          if (TrcWalkManager.RoughEqual(middleContext.GetGlobalTargetPosition(middle),
                                        farContext.GetGlobalTargetPosition(far)))
          {
            if (FallenThroughSource)
            {
              mySrcPointYToTheirTargetY.Add(new WalkSegmentView(globalContext, middleContext, middle));
              mySrcPointYToTheirTargetY.Add(new WalkSegmentView(globalContext, farContext, far));
            }
            else
            {
              myTargetYToTheirTargetY.Add(new WalkSegmentView(globalContext, middleContext, middle));
              myTargetYToTheirTargetY.Add(new WalkSegmentView(globalContext, farContext, far));
            }
          }
        }
        if (jmpAgain)
        {
          if (far.Direction.OtherWalk != null && far.Direction.OtherWalk != middleContext)
          {
            fallenInFollowingAtLeastOneIndirection(far.Direction.OtherWalk, farContext, FallenThroughSource);
          }
        }
      }
    }
    private float getFloatFromString(string value)
    {
      string clreasStr = value.Replace(',', '.');
      clreasStr = new string(clreasStr.Where(x => char.IsDigit(x) || x == '.').ToArray());
      float val = 0.0f;
      try
      {
        val = float.Parse(clreasStr);
      }
      catch (Exception) { }
      return val;
    }
  }
}
